// PX2Scene.inl

//----------------------------------------------------------------------------
inline EnvirParam *Scene::GetEnvirParam()
{
	return mEnvirParam;
}
//----------------------------------------------------------------------------
inline std::vector<CameraPtr> &Scene::GetCameras()
{
	return mCameras;
}
//----------------------------------------------------------------------------