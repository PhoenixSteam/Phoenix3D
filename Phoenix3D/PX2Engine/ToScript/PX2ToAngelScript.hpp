// PX2ToAngelScript.hpp

#ifndef PX2TOANGELSCRIPT_HPP
#define PX2TOANGELSCRIPT_HPP

#include "PX2Core.hpp"
#include "PX2Mathematics.hpp"
#include "PX2Graphics.hpp"
#include "PX2Renderers.hpp"
#include "PX2Unity.hpp"
#include "PX2Terrains.hpp"
#include "PX2Effect.hpp"
#include "PX2UI.hpp"
#include "PX2Applications.hpp"
using namespace PX2;

PX2_ENGINE_ITEM int toAS_PX2_open(asIScriptEngine* asEngine);

#endif